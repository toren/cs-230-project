# %% [code]
#######################
### Library imports ###
#######################

# standard library
import os
import sys
import pickle
import copy

# data packages
import numpy as np
import pandas as pd

# pytorch
import torch
import torch.nn as nn

# sklearn 
import sklearn.metrics

# custom tooling
sys.path.append("../input/utilities")
import models
import splitting
import preprocess

########################
### Global variables ###
########################
label_smoothing = True
smoothing = 0.001
device = ("cuda" if torch.cuda.is_available() else "cpu")
os.environ["CUDA_LAUNCH_BLOCKING"] = "1"

# %% [code]
###################
### Import Data ###
###################

train_drug = pd.read_csv("../input/lish-moa/train_drug.csv")
X = pd.read_csv("../input/lish-moa/train_features.csv")
y = pd.read_csv("../input/lish-moa/train_targets_scored.csv")
X_test = pd.read_csv("../input/lish-moa/test_features.csv")
y_test = pd.read_csv("../input/test-targets/solution.csv")
y_test = y_test[y_test['Usage'] == 'Public']
# Remove 'Usage' column
y_test.drop('Usage', axis=1, inplace=True)
drugs = pd.read_csv("../input/lish-moa/train_drug.csv")

# %% [code]
##########################
### Data Preprocessing ###
##########################

transformer = preprocess.Preprocessor() 
transformer.fit(X)
X = transformer.transform(X)
X_test = transformer.transform(X_test)
y = y.drop(["sig_id"], axis = 1).values.astype("float32")
y_test = y_test.drop(["sig_id"], axis = 1).values.astype("float32")

# %% [code]
# Define network architecture 

n_input = X.shape[1]
n_output = y.shape[1]
hidden_units = 256
dropout = 0.4

net_obj = models.Sequential(
    nn.Linear(n_input, hidden_units),
    nn.BatchNorm1d(hidden_units),
    nn.LeakyReLU(),
    nn.Dropout(dropout),
    nn.Linear(hidden_units, hidden_units),
    nn.BatchNorm1d(hidden_units),
    nn.LeakyReLU(),
    nn.Dropout(dropout),
    nn.Linear(hidden_units, n_output)
)

# %% [code]
log_loss = nn.BCEWithLogitsLoss()
smoothed_log_loss = models.SmoothCrossEntropyLoss(smoothing=smoothing, device=device)

if label_smoothing:
    loss = smoothed_log_loss
else:
    loss = log_loss

# Initialize network
net = models.Network(
    net_obj=net_obj, 
    max_epochs=20,
    batch_size=128, 
    device=device,
    loss_fn=loss, 
    lr=0.01,
    weight_decay=0.00001,
    lr_scheduler="ReduceLROnPlateau",
    seed=2021
)

net.fit(
    X,
    y,
    eval_set=[(X_test, y_test)],
    eval_names=['test'],
    eval_metric=[log_loss],
    verbose=1
)

# %% [code]
net.metric_history_df_.tail(4)

# %% [code]
# Evaluation 

def multi_log_loss(y_pred, y_true):
    losses = -y_true * np.log(y_pred + 1e-15) - (1 - y_true) * np.log(1 - y_pred + 1e-15)
    return np.mean(losses)

preds = net.predict_proba(X)
test_probs = net.predict_proba(X_test)

print("Train loss: ", multi_log_loss(preds, y))
print("Test loss: ", multi_log_loss(test_probs, y_test))

pd.DataFrame(test_probs).to_csv("NN_test_probs.csv", index=False)
