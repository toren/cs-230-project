# %% [code]
# standard library
import os
import sys

# data packages
import numpy as np
import pandas as pd

# lightgbm
import lightgbm as lgb

# parallelization
from joblib import Parallel, delayed

# custom tooling
sys.path.append("../input/utilities")
import models
import splitting
import preprocess

# %% [code]
###################
### Import Data ###
###################

X = pd.read_csv("../input/lish-moa/train_features.csv")
y = pd.read_csv("../input/lish-moa/train_targets_scored.csv")
X_test = pd.read_csv("../input/lish-moa/test_features.csv")
y_test = pd.read_csv("../input/test-targets/solution.csv")
y_test = y_test[y_test['Usage'] == 'Public']
# Remove 'Usage' column
y_test.drop('Usage', axis=1, inplace=True)
drugs = pd.read_csv("../input/lish-moa/train_drug.csv")

# %% [code]
##########################
### Data Preprocessing ###
##########################

transformer = preprocess.Preprocessor() 
transformer.fit(X)
X = transformer.transform(X)
X_test = transformer.transform(X_test)
y = y.drop(["sig_id"], axis = 1).values.astype("float32")
y_test = y_test.drop(["sig_id"], axis = 1).values.astype("float32")

# %% [code]
lgb_params = {
    "num_leaves": 50,
    "max_depth": 4,
    "learning_rate": 0.01,
    "n_estimators": 100,
    "min_child_weight": 0.02,
    "objective": "binary",
    "random_state": 2021
}

# %% [code]
# fit 206 models (one for each label)
model = models.OVRModel(lgb.LGBMClassifier(**lgb_params), n_jobs=-2)

model.fit(X=pd.DataFrame(X), y=pd.DataFrame(y))

# %% [code]
preds = model.predict_proba(X)
test_probs = model.predict_proba(X_test)

# %% [code]
def multi_log_loss(y_pred, y_true):
    losses = -y_true * np.log(y_pred + 1e-15) - (1 - y_true) * np.log(1 - y_pred + 1e-15)
    return np.mean(losses)

print("Train loss: ", multi_log_loss(preds, y))
print("Test loss: ", multi_log_loss(test_probs, y_test))

pd.DataFrame(test_probs).to_csv("GB_test_probs.csv", index=False)
