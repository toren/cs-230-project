#######################
### Library imports ###
#######################

# standard library
import copy
import os

# data packages
import numpy as np
import pandas as pd



def split_on_group(n_splits, groups, random_state=None):
    """Custom splitting on groups.
    Parameters
    ----------
    n_splits : int
        Number of splits (folds) to make.
    groups : array-like
        The group labels for every observation.
    random_state : int, optional
        Random state for reproducibility.
    Returns
    -------
    list of tuple
        The split indices as a list of tuples of `(idx_train, idx_test)`.
    Notes
    -----
    Adapted from https://stackoverflow.com/a/54254277.
    """

    groups = pd.Series(groups)
    ix = np.arange(len(groups))
    unique = np.unique(groups)
    np.random.RandomState(random_state).shuffle(unique)
    result = []
    for split in np.array_split(unique, n_splits):
        mask = groups.isin(split)
        train, test = ix[~mask], ix[mask]
        result.append((train, test))

    return result


def create_splitting_groups(df, strat_vars, split_var, random_state=None):
    """Create custom MoA-based groups for CV splitting.
    Parameters
    ----------
    df : pd.DataFrame
        Input data, containing all columns in `strat_vars` and `split_var`.
    strat_vars : list of str
        Variables to use for stratification, e.g. ["cp_time", "cp_dose"].
    split_var : str
        Variable used for demarcating splits, e.g. "drug_id".
    random_state : int
        Random state for reproducibiity.
    Returns
    -------
    pd.Series
        The constructed groups.
    """

    df_groups = df.assign(
        # add random noise to do random ranking
        random=lambda x: np.random.RandomState(random_state).rand(x.shape[0]),
        # randomly rank (i.e. assign ids) within group
        rank=lambda x: x.groupby(strat_vars + [split_var])["random"]
        # ties shouldn't occur, but make sure distinct ranks are assigned if they do
        .rank(method="first").astype(int),
        # then group is based on this rank and the splitting variable (str concat)
        group=lambda x: x[[split_var, "rank"]]
        .astype(str)
        .apply(lambda y: "_".join(y), axis=1),
    )

    # return the pd.Series of the new groups (with original df index)
    return df_groups["group"]
