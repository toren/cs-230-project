# %% [code]
# standard library
import os
import sys

# data packages
import numpy as np
import pandas as pd

# lightgbm
import lightgbm as lgb

# parallelization
from joblib import Parallel, delayed

# custom tooling
sys.path.append("../input/utilities")
import models
import splitting
import preprocess

########################
### Global variables ###
########################

n_seeds = 1
n_folds = 5

# %% [code]
import sklearn.base
import sklearn.model_selection

# Create modified grid search
# This grid search is similar to that in `models.py` but it works with `OVRModels` and LightGBM.

class GridSearch(sklearn.base.BaseEstimator):
    """Custom analogue to sklearn's GridSearchCV with support for early stopping.
    Parameters
    ----------
    model : obj
    param_grid : dict of str to list
        Dictionary mapping each parameter to a list of candidate values to be searched.
    loss_fn : callable
        A function or callable loss object with signature `loss_fn(y_pred, y_true)`.
    Attributes
    ----------
    results_ : list of dict
        List of dictionaries recording fold, parameters, and loss from the grid search.
    results_df_ : pd.DataFrame
        A dataframe wrapper around `self.results_`.
    """

    def __init__(self, model, param_grid, loss_fn):
        self.model = model
        self.param_grid = param_grid
        self.loss_fn = loss_fn

    def fit(self, X, y=None, splits=None, **fit_params):

        """Split data into folds and train/evaluate parameter combinations on each fold.
        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The training features.
        y : array-like, shape (n_samples, n_labels), optional
            The training labels, if required.
        splits : list of tuple
            The fold splits to  be used in training, in the form of a list of
            `(idx_train, idx_test)` tuples.
        **fit_params
            Additional fit parameters to pass to the model.
        Returns
        -------
        self : obj
            Returns the estimator itself.
        Notes
        -----
        This estimator does not save the trained models or implement a `best_model_`
        attribute or `predict` method. It is intended only for obtaining optimal
        parameter combos, most easily accessed in the `results_df_` attribtue.
        """

        loss_fn = (
            self.loss_fn if isinstance(self.loss_fn, dict) else {"loss": self.loss_fn}
        )

        # create parameter grid (result is list of parameter dicts)
        pg = sklearn.model_selection.ParameterGrid(self.param_grid)

        # we'll record results for every param combo and fold
        self.results_ = []
        for params in pg:
            self.model.model = sklearn.base.clone(self.model.model).set_params(**params)
            # use our custom CV implementation to pass oos fold as val set
            cv = models.CVModel(model)
            cv.fit(X=X, y=y, splits=splits, **fit_params)
            # get oof error for every fold
            for i, m in enumerate(cv.models_):
                oof_indices = splits[i][1]
                y_pred = m.predict(models._subset_rows(X, oof_indices))
                y_true = models._subset_rows(y, oof_indices)
                loss = {name: f(y_pred, y_true).item() for name, f in loss_fn.items()}
                # extract the name for network class so output is more readable
                params_copy = params.copy()
                self.results_.append({"fold": i, **params_copy, **loss})

        # add dataframe version to easily read results
        self.results_df_ = pd.DataFrame(self.results_)

        return self

# %% [code]
###################
### Import Data ###
###################

X = pd.read_csv("../input/lish-moa/train_features.csv")
y = pd.read_csv("../input/lish-moa/train_targets_scored.csv")
drugs = pd.read_csv("../input/lish-moa/train_drug.csv")

# %% [code]
# Add drugs for k-fold splitting
X = X.merge(drugs, on = 'sig_id', how = 'inner')

# create k-fold splits 
groups = splitting.create_splitting_groups(
    X, 
    strat_vars=["cp_dose", "cp_time"], 
    split_var="drug_id", 
    random_state=2021
)

splits = splitting.split_on_group(n_splits=n_folds, groups=groups, random_state=2021)

X.drop('drug_id', axis=1, inplace=True)

# %% [code]
##########################
### Data Preprocessing ###
##########################

transformer = preprocess.Preprocessor() 
transformer.fit(X)
X = transformer.transform(X)
y = y.drop(["sig_id"], axis = 1).values.astype("float32")

# %% [code]
# NOTE: need to specify params, but these are overwritten 
# by param_grid in grid search.
lgb_params = {
    "num_leaves": 10,
    "learning_rate": 0.05,
    "n_estimators": 100,
    "objective": "binary",
    "random_state": 2021
}

param_grid = {
    "num_leaves": [10, 50, 1000],
    "max_depth": [3, 4, 10],
    "learning_rate": [0.01, 0.001],
}

# %% [code]
def multi_log_loss(y_pred, y_true):
    losses = -y_true * np.log(y_pred + 1e-15) - (1 - y_true) * np.log(1 - y_pred + 1e-15)
    return np.mean(losses)

# %% [code]
model = models.OVRModel(lgb.LGBMClassifier(**lgb_params), n_jobs=-2)

gs = GridSearch(
    model=model,
    param_grid=param_grid,
    loss_fn=multi_log_loss
)

gs.fit(
    X=X, 
    y=y,
    splits=splits
)

# %% [code]
# Get loss for each param combo
df_results = gs.results_df_

# Average over folds and seeds
param_cols = [col for col in list(df_results.columns) if col not in ["fold", "seed", "loss"]]
df_results_params = (
    df_results
    .groupby(param_cols, as_index=False)["loss"]
    .mean()
    .sort_values(by=["loss"]) # order by lowest to highest loss
)

df_results_params.head(10)
