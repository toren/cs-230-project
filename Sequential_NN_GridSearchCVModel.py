# %% [code]
#######################
### Library imports ###
#######################

# standard library
import os
import sys
import pickle
import copy

# data packages
import numpy as np
import pandas as pd

# pytorch
import torch
import torch.nn as nn

import joblib
import itertools

# custom tooling
sys.path.append("../input/utilities")
import models
import splitting
import preprocess

########################
### Global variables ###
########################

label_smoothing = True
smoothing = 0.001
n_seeds = 1
n_folds = 5
device = ("cuda" if torch.cuda.is_available() else "cpu")
os.environ["CUDA_LAUNCH_BLOCKING"] = "1"

# %% [code]
# creates nets for grid search
def create_nets(n_input, n_output, nodes_list, dropout_list, batch_norm_list):
    nets = []
    for nodes, dropout, batch_norm in itertools.product(nodes_list, dropout_list, batch_norm_list):
        if batch_norm:
            net_obj = models.Sequential(
                nn.Linear(n_input, nodes),
                nn.BatchNorm1d(nodes),
                nn.LeakyReLU(),
                nn.Dropout(dropout),
                nn.Linear(nodes, nodes),
                nn.BatchNorm1d(nodes),
                nn.LeakyReLU(),
                nn.Dropout(dropout),
                nn.Linear(nodes, n_output)
            )
        else:
            net_obj = models.Sequential(
                nn.Linear(n_input, nodes),
                nn.LeakyReLU(),
                nn.Dropout(dropout),
                nn.Linear(nodes, nodes),
                nn.LeakyReLU(),
                nn.Dropout(dropout),
                nn.Linear(nodes, n_output)
            )
        net_obj.name = f"Nodes: {nodes}, Dropout: {dropout}, Batch Norm: {batch_norm}"
        nets.append(net_obj)
    
    return nets

# %% [code]
###################
### Import Data ###
###################

X = pd.read_csv("../input/lish-moa/train_features.csv")
y = pd.read_csv("../input/lish-moa/train_targets_scored.csv")
drugs = pd.read_csv("../input/lish-moa/train_drug.csv")

# %% [code]
def create_y_type(y):
    y = y.rename(columns={col: "val_" + col for col in y.columns if col not in ["sig_id"]})
    y_long = pd.wide_to_long(
        y, stubnames="val", i="sig_id", j="moa", sep="_", suffix=".*"
    ).reset_index()
    # get suffix. This is the interaction (e.g. agonist, inhibitor, etc.)
    y_long["interactions"] = [x.rsplit("_", 1)[-1] for x in y_long["moa"]]
    y_long.drop("moa", axis=1, inplace=True)
    # keep all interactions that appear more than once, all others set to "other"
    interactions = y_long["interactions"].value_counts()[:6].index.tolist()
    y_long.loc[~y_long["interactions"].isin(interactions), "interactions"] = "other"
    # if any enzyme or protein has a given interaction, set interaction to 1, else 0
    y_long = y_long.groupby(["sig_id", "interactions"]).max().reset_index()
    # pivot wider
    y_type = pd.pivot(
        y_long, index = "sig_id", columns = "interactions", values = "val"
    ).reset_index()
    return y_type

y_type = create_y_type(y)

# %% [code]
# Add drugs for k-fold splitting
X = X.merge(drugs, on = 'sig_id', how = 'inner')

# create k-fold splits 
groups = splitting.create_splitting_groups(
    X, 
    strat_vars=["cp_dose", "cp_time"], 
    split_var="drug_id", 
    random_state=2021
)

splits = splitting.split_on_group(n_splits=n_folds, groups=groups, random_state=2021)

X.drop('drug_id', axis=1, inplace=True)

# %% [code]
##########################
### Data Preprocessing ###
##########################

transformer = preprocess.Preprocessor() 
transformer.fit(X)
X = transformer.transform(X)
y = y.drop(["sig_id"], axis = 1).values.astype("float32")
y_type = y_type.drop(["sig_id"], axis = 1).values.astype("float32")

# %% [code]
#########################
### First-Stage Model ###
#########################

log_loss = nn.BCEWithLogitsLoss()
smoothed_log_loss = models.SmoothCrossEntropyLoss(smoothing=smoothing, device=device)

if label_smoothing:
    loss = smoothed_log_loss
else:
    loss = log_loss

# Define network architecture 

n_input = X.shape[1]
n_output = y_type.shape[1]
# NOTE: need to specify params, but these are overwritten 
# by param_grid in grid search.
hidden_units = 256
dropout = 0.4

net_obj = models.Sequential(
    nn.Linear(n_input, hidden_units),
    nn.BatchNorm1d(hidden_units),
    nn.LeakyReLU(),
    nn.Dropout(dropout),
    nn.Linear(hidden_units, hidden_units),
    nn.BatchNorm1d(hidden_units),
    nn.LeakyReLU(),
    nn.Dropout(dropout),
    nn.Linear(hidden_units, n_output)
)

# Initialize network
# NOTE: need to specify params, but these are overwritten 
# by param_grid in grid search.
net = models.Network(
    net_obj=net_obj, 
    max_epochs=20,
    batch_size=128, 
    device=device,
    loss_fn=loss, 
    lr=0.01,
    weight_decay=1e-6,
    lr_scheduler="ReduceLROnPlateau"
)

seeds = range(2021, 2021 + n_seeds)

param_grid = {
    "net_obj": create_nets(
        n_input=X.shape[1],
        n_output=y_type.shape[1],
        nodes_list=[128, 256, 512],
        dropout_list=[0, 0.2, 0.3, 0.4],
        batch_norm_list=[True]
    ),
    "seed": seeds,
    "lr": [0.01, 0.001],
    "weight_decay": [1e-4, 1e-5, 1e-6]    
}

gs = models.GridSearch(
    model=net,
    param_grid=param_grid,
    loss_fn=loss
)

gs.fit(
    X=X, 
    y=y_type,
    splits=splits,
    eval_metric=[log_loss], 
    verbose=10
)

# Get loss for each param combo
df_results = gs.results_df_

# Average over folds and seeds
param_cols = [col for col in list(df_results.columns) if col not in ["fold", "seed", "loss"]]
df_results_params = (
    df_results
    .groupby(param_cols, as_index=False)["loss"]
    .mean()
    .sort_values(by=["loss"])
)

# Get parameters for best model
best_model = df_results_params.iloc[0]
best_net_obj = dict((x.strip(), y.strip()) 
                 for x, y in (element.split(':')  
                 for element in best_model["net_obj"].split(', ')))

df_results_params.head(10)

# %% [code]
# Re-fit best model to use as inputs in grid search for second-stage model

hidden_units = int(best_net_obj["Nodes"])
dropout = float(best_net_obj["Dropout"])

net_obj = models.Sequential(
    nn.Linear(n_input, hidden_units),
    nn.BatchNorm1d(hidden_units),
    nn.LeakyReLU(),
    nn.Dropout(dropout),
    nn.Linear(hidden_units, hidden_units),
    nn.BatchNorm1d(hidden_units),
    nn.LeakyReLU(),
    nn.Dropout(dropout),
    nn.Linear(hidden_units, n_output)
)

# Initialize network
net = models.Network(
    net_obj=net_obj, 
    max_epochs=20,
    batch_size=128, 
    device=device,
    loss_fn=loss, 
    lr=best_model["lr"],
    weight_decay=best_model["weight_decay"],
    lr_scheduler="ReduceLROnPlateau"
)

net.fit(
    X,
    y_type,
    eval_metric=[log_loss],
    verbose=10
)

y_type_preds = net.predict_proba(X)

# %% [code]
##########################
### Second-Stage Model ###
##########################

X = np.column_stack((X, y_type_preds))

# Initialize network
# NOTE: need to specify params, but these are overwritten 
# by param_grid in grid search.
net = models.Network(
    net_obj=net_obj, 
    max_epochs=20,
    batch_size=128, 
    device=device,
    loss_fn=loss, 
    lr=0.01,
    weight_decay=1e-6,
    lr_scheduler="ReduceLROnPlateau"
)

param_grid = {
    "net_obj": create_nets(
        n_input=X.shape[1],
        n_output=y.shape[1],
        nodes_list=[128, 256, 512],
        dropout_list=[0, 0.2, 0.3, 0.4],
        batch_norm_list=[True, False]
    ),
    "seed": seeds,
    "lr": [0.01, 0.001],
    "weight_decay": [1e-4, 1e-5, 1e-6]    
}

gs = models.GridSearch(
    model=net,
    param_grid=param_grid,
    loss_fn=loss
)

gs.fit(
    X=X, 
    y=y,
    splits=splits,
    eval_metric=[loss], 
    verbose=10
)

# %% [code]
# Get loss for each param combo
df_results = gs.results_df_

# Average over folds and seeds
param_cols = [col for col in list(df_results.columns) if col not in ["fold", "seed", "loss"]]
df_results_params = (
    df_results
    .groupby(param_cols, as_index=False)["loss"]
    .mean()
    .sort_values(by=["loss"])
)

df_results_params.head(10)
